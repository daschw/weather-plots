mod args;
mod meteo;

use anyhow::Result;
use args::Args;
use chrono::Utc;
use clap::Parser;
use meteo::{Meteo, MeteoSeries};
use plotters::{coord::Shift, prelude::*};

/// Parse the arguments, fetch meteo data and plot.
#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();
    let (lat, lon) = args.coords()?;
    let meteo = Meteo::new(lat, lon).await?;
    plot(&args, &meteo)
}

/// Create a drawing area, split it in three and add a series for the temperature, the wind
/// speed and the precipitation probability to each of the sub-areas.
/// Use the colors in `args`, finally save the figure to the file specified in `args` and
/// return the filename.
fn plot(args: &Args, meteo: &Meteo) -> Result<()> {
    let output = args.output()?;
    let area = BitMapBackend::new(&output, (1080, 640)).into_drawing_area();
    area.fill(&args.bg()?)?;
    let chart_areas = area.split_evenly((3, 1));
    draw_series(
        chart_areas.first().unwrap(),
        &meteo.temperature_series(args.temperature_color()?)?,
        args,
    )?;
    draw_series(
        chart_areas.get(1).unwrap(),
        &meteo.wind_speed_series(args.wind_color()?)?,
        args,
    )?;
    draw_series(
        chart_areas.last().unwrap(),
        &meteo.precipitation_probability_series(args.precipitation_color()?)?,
        args,
    )?;
    area.present()?;
    if args.verbose {
        println!("{:#?}", output)
    };
    Ok(())
}

/// Add a single `series` to a drawing `area` with the user input `args`.
/// This configures the mesh of `area` with the proper styles and formatters and adds a
/// semi-tranparent area as well as a line plot with the data and color provided in
/// `series`to `area`.
/// Furthermore, it draws a vertical line indicating the current date and time.
fn draw_series(
    area: &DrawingArea<BitMapBackend, Shift>,
    series: &MeteoSeries,
    args: &Args,
) -> Result<()> {
    let fg = &args.fg()?;
    let label_style = ("sans-serif", 20, fg).into_text_style(area);
    let start_date = *series.time.first().unwrap();
    let end_date = *series.time.last().unwrap();
    let (min_value, max_value) = extrema_with_start(&series.value, series.lims);
    let mut chart = ChartBuilder::on(area)
        .margin(25)
        .x_label_area_size(25)
        .y_label_area_size(75)
        .build_cartesian_2d(start_date..end_date, min_value..max_value)?;
    chart
        .configure_mesh()
        .axis_style(args.bg()?)
        .label_style(label_style)
        .light_line_style(args.fg()?.mix(0.12))
        .bold_line_style(args.fg()?.mix(0.12))
        .x_max_light_lines(6)
        .y_max_light_lines(1)
        .x_label_formatter(&|datetime| format!("{}", datetime.format("%a, %e")))
        .y_label_formatter(&|value| format!("{}", *value as i32))
        .y_desc(series.label.clone())
        .y_labels(8)
        .draw()?;
    chart.draw_series(LineSeries::new(
        [(Utc::now(), min_value), (Utc::now(), max_value)],
        args.now_color()?,
    ))?;
    chart.draw_series(
        AreaSeries::new(
            series
                .time
                .iter()
                .copied()
                .zip(series.value.iter().copied()),
            0.0,
            series.color.mix(0.2),
        )
        .border_style(series.color.stroke_width(2)),
    )?;
    Ok(())
}

/// Find the extrema in `values` starting with `(min, max) = start`.
/// This returns them minimum of `min` and all elements in `values` and the maximum of
/// `max` and all elements in `values`.
fn extrema_with_start(values: &[f32], start: (f32, f32)) -> (f32, f32) {
    let mut extrema = start;
    for value in values.iter() {
        if value < &extrema.0 {
            extrema.0 = *value;
        } else if value > &extrema.1 {
            extrema.1 = *value;
        }
    }
    extrema
}
