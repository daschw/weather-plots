use anyhow::Result;
use chrono::{DateTime, NaiveDateTime, TimeZone, Utc};
use plotters::style::RGBAColor;
use serde::Deserialize;

/// A utility struct to deserialize the hourly_units field in the JSON object returned in
/// the open-meteo.com API call.
///
/// We could use the following fields:
///
///     - time: String
///     - temperature_2m: String
///     - precipitation_probability: String
///     - wind_speed_10m: String
///
/// However, we only use the ones we need.
#[derive(Debug, Deserialize)]
struct OpenMeteoUnits {
    temperature_2m: String,
    precipitation_probability: String,
    wind_speed_10m: String,
}

/// A utility struct to deserialize the hourly field in the JSON object returned in the
/// open-meteo.com API call.
///
/// It is characterized by the following fields:
///
///     - time: Vec<String>
///     - temperature_2m: Vec<f32>
///     - precipitation_probability: Vec<f32>
///     - wind_speed_10m: Vec<f32>
#[derive(Debug, Deserialize)]
struct OpenMeteoSeries {
    time: Vec<String>,
    temperature_2m: Vec<f32>,
    precipitation_probability: Vec<f32>,
    wind_speed_10m: Vec<f32>,
}

/// A struct holding all the information required to plot a series of a `Meteo` object.
#[derive(Debug)]
pub struct MeteoSeries {
    pub time: Vec<DateTime<Utc>>,
    pub value: Vec<f32>,
    pub label: String,
    pub color: RGBAColor,
    pub lims: (f32, f32),
}

/// A struct to deserialize the JSON object returned in the open-meteo.com API call.
///
/// We could use the following fields:
///
///     - latitude: f32
///     - longitude: f32
///     - generationtime_ms: f32
///     - utc_offset_seconds: usize
///     - timezone: String
///     - timezone_abbreviation: String
///     - elevation: f32
///     - hourly_units: OpenMeteoUnits
///     - hourly: OpenMeteoSeries
///
/// However, we only use the ones we need.
#[derive(Debug, Deserialize)]
pub struct Meteo {
    hourly_units: OpenMeteoUnits,
    hourly: OpenMeteoSeries,
}

impl Meteo {
    /// Fetch weather forecast data from open-meteo.com an deserialize the response JSON to
    /// a `Meteo` object.
    pub async fn new(lat: f32, lon: f32) -> Result<Meteo> {
        Ok(reqwest::get(format!(
            "{}?latitude={}&longitude={}&{}&{}&{}",
            "https://api.open-meteo.com/v1/forecast",
            lat,
            lon,
            "hourly=temperature_2m,precipitation_probability,wind_speed_10m",
            "timezone=Europe/Vienna",
            "past_days=3",
        ))
        .await?
        .json::<Meteo>()
        .await?)
    }

    /// Convert the strings from `time` in `hourly` to `DateTime<Utc>`.
    pub fn time(&self) -> Result<Vec<DateTime<Utc>>> {
        self.hourly
            .time
            .iter()
            .map(|s| Ok(Utc.from_utc_datetime(&NaiveDateTime::parse_from_str(s, "%FT%R")?)))
            .collect()
    }

    /// Get the temperature series of a `Meteo` object holding information about the
    /// temperature at two meters above the ground in °C for the past three and upcoming
    /// seven days.
    pub fn temperature_series(&self, color: RGBAColor) -> Result<MeteoSeries> {
        Ok(MeteoSeries {
            time: self.time()?,
            value: self.hourly.temperature_2m.clone(),
            label: format!("Temperature ({})", self.hourly_units.temperature_2m),
            color,
            lims: (0.0, 0.0),
        })
    }

    /// Get the wind speed series of a `Meteo` object holding information about the wind
    /// speed at ten meters above the ground in km/h for the past three and upcoming seven
    /// days.
    pub fn wind_speed_series(&self, color: RGBAColor) -> Result<MeteoSeries> {
        Ok(MeteoSeries {
            time: self.time()?,
            value: self.hourly.wind_speed_10m.clone(),
            label: format!("Wind Speed ({})", self.hourly_units.wind_speed_10m),
            color,
            lims: (0.0, 0.0),
        })
    }

    /// Get the precipitation probability series of a `Meteo` object holding information
    /// about the precipitation probability in % for the past three and upcoming seven
    /// days.
    pub fn precipitation_probability_series(&self, color: RGBAColor) -> Result<MeteoSeries> {
        Ok(MeteoSeries {
            time: self.time()?,
            value: self.hourly.precipitation_probability.clone(),
            label: format!(
                "Precipitation prob. ({})",
                self.hourly_units.precipitation_probability
            ),
            color,
            lims: (0.0, 100.0),
        })
    }
}
