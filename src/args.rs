use anyhow::{anyhow, Result};
use clap::Parser;
use plotters::style::RGBAColor;
use std::{path::PathBuf, str::FromStr};

/// Create a nice plot with weather forecasts.
#[derive(Parser)]
#[command(
    author,
    version,
    about,
    long_about = "

    weather-plots

With weather-plots you can fetch weather forecast data for specific cities from
https://open-meteo.com/ and create a plot showing the past three days and the forecast for
the upcoming seven days for

    - the temperature,
    - the wind speed, and
    - the precipitation probability.

Currently, only five cities are supported.
However, I plan to support providing arbitray location specifications with `--lat` and
`--lon` arguments or something similar.

By default, weather-plots uses the colors from the
[leaf](https://github.com/daschw/leaf.nvim) dark theme.
However, colors can be modified by the user.
Supported color formats are `#RRGGBB` and `#RRGGBBAA`.
To create a plot for Wien with the cattpucchin frappe theme you can run

    weather-plots -c=wien -b=#303446 -f=#c6d0f5 -t=#e5c890 -w=#81c8be -p=#8caaee -n=#ca9ee6

If no output file name is provided by the user the plot is saved under
`$XDG_DATA_DIR/weather-plots/$CITY.png`.
In the example above on linux this would be `~/.local/share/weather-plots/wien.png`.
"
)]
pub struct Args {
    /// Name of the city (wien, kematen, haiding, scharnstein, timisoara)
    #[arg(short, long)]
    pub city: String,
    /// Background color
    #[arg(short, long, value_name = "#RRGGBB[AA]")]
    bg: Option<String>,
    /// Foreground color
    #[arg(short, long, value_name = "#RRGGBB[AA]")]
    fg: Option<String>,
    /// Color of the temperature series
    #[arg(short, long, value_name = "#RRGGBB[AA]")]
    temperature: Option<String>,
    /// Color of the wind speed series
    #[arg(short, long, value_name = "#RRGGBB[AA]")]
    wind: Option<String>,
    /// Color of the precipitation probability series
    #[arg(short, long, value_name = "#RRGGBB[AA]")]
    precipitation: Option<String>,
    /// Color of the vertical line indicating the current time
    #[arg(short, long, value_name = "#RRGGBB[AA]")]
    now: Option<String>,
    /// Output file
    #[arg(short, long, value_name = "FILE")]
    output: Option<String>,
    /// Print the path to the output file
    #[arg(short, long, value_name = "BOOL")]
    pub verbose: bool,
}

impl Args {
    /// Get the coordinates `(lat, lon)` of the city provided by the user.
    pub fn coords(&self) -> Result<(f32, f32)> {
        match &self.city as &str {
            "wien" => Ok((48.2085, 16.3721)),
            "kematen" => Ok((48.1765, 13.8587)),
            "timisoara" => Ok((45.7537, 21.2257)),
            "haiding" => Ok((48.21, 13.9718)),
            "scharnstein" => Ok((47.9043, 13.9613)),
            _ => Err(anyhow!(
                "There are no coordinates defined yet for {}",
                self.city
            )),
        }
    }

    /// Get the background color provided by the user falling back to leaf dark theme
    /// background.
    pub fn bg(&self) -> Result<RGBAColor> {
        rgba_with_default(&self.bg, "#2E2C2FEA")
    }

    /// Get the Foreground color provided by the user falling back to leaf dark theme
    /// foreground.
    pub fn fg(&self) -> Result<RGBAColor> {
        rgba_with_default(&self.fg, "#E1E4DC")
    }

    /// Get the color associated to the temperature series provided by the user falling
    /// back to leaf dark theme yellow.
    pub fn temperature_color(&self) -> Result<RGBAColor> {
        rgba_with_default(&self.temperature, "#CCAA6C")
    }

    /// Get the color associated to the wind speed series provided by the user falling
    /// back to leaf dark theme green.
    pub fn wind_color(&self) -> Result<RGBAColor> {
        rgba_with_default(&self.wind, "#729B79")
    }

    /// Get the color associated to the precipitation probability series provided by the
    /// user falling back to leaf dark theme blue.
    pub fn precipitation_color(&self) -> Result<RGBAColor> {
        rgba_with_default(&self.precipitation, "#5292C6")
    }

    /// Get the color of the vertical line showing the current time provided by the user
    /// falling back to leaf dark theme purple.
    pub fn now_color(&self) -> Result<RGBAColor> {
        rgba_with_default(&self.now, "#8C6AA8")
    }

    /// Get the output filename provided by the user falling back to
    /// $XDG_DATA_DIR/weather-plots/$CITY.png.
    pub fn output(&self) -> Result<PathBuf> {
        match &self.output {
            Some(filepath) => Ok(PathBuf::from_str(filepath)?),
            _ => Ok(xdg::BaseDirectories::with_prefix("weather-plots")?
                .place_data_file(format!("{}.png", self.city))?),
        }
    }
}

/// Get the `RGBAColor` from an optional user input string or a default string.
fn rgba_with_default(input: &Option<String>, default: &str) -> Result<RGBAColor> {
    match input {
        Some(hexcode) => rgba(hexcode),
        _ => rgba(default),
    }
}

/// Parse a hexcode string provided in the form "RRGGBB[AA]" as `RGBAColor`.
fn rgba(hexcode: &str) -> Result<RGBAColor> {
    let n = hexcode.len();
    if !hexcode.starts_with('#') || (n != 7 && n != 9) {
        return Err(anyhow!("\"{}\" is not a valid color.", hexcode));
    }
    let r: u8 = u8::from_str_radix(&hexcode[1..3], 16)?;
    let g: u8 = u8::from_str_radix(&hexcode[3..5], 16)?;
    let b: u8 = u8::from_str_radix(&hexcode[5..7], 16)?;
    let a: f64 = if n == 9 {
        u8::from_str_radix(&hexcode[7..9], 16)? as f64 / 255.0
    } else {
        1.0
    };
    Ok(RGBAColor(r, g, b, a))
}
