# weather-plots

Fetch weather forecast for specific cities and produce some nice plots.

![wien.png](https://codeberg.org/attachments/660469ee-c3d2-49b7-ab01-5611bb356ea2)

## Install

### Nix Flakes with  Home-Manager

Add the url to your flake inputs.

```nix
inputs.weather-plots.url = "git+https://codeberg.org/daschw/weather-plots";
```

Add the overlay to nixpkgs in your home config.

```nix
nixpkgs.overlays = [
    inputs.weather-plots.overlays.default
];
```

Now you can add the package to your home-manager config.

```nix
home.packages = [
    weather-plots
];
```

## Usage

```sh
weather-plots --help
```

To create a weather forecast plot for `wien` with the
[catppuccin](https://github.com/catppuccin/catppuccin) frappe colors you can run the
following command.
By default the file will be saved at `~/.local/share/weather-plots/wien.png`.
You can change this by adding the `-o=FILE` argument to the command below.

```
weather-plots -c=wien -b=#303446 -f=#c6d0f5 -t=#e5c890 -w=#81c8be -p=#8caaee -n=#ca9ee6
```

![catppuccin](https://codeberg.org/attachments/b914127d-a235-4fe5-8ded-c3eeb7e5cd9d)
